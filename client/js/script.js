"use strict";

var SERVER_URL = 'https://server-prod-259466e4-e37a-47db-a567-51d72e6df86b.voxis.dev';
// var SERVER_URL = `https://server-prod-fc05e48e-4209-4f74-b6ae-057035a074bc.voxis.dev`;

function sendMessage() {
  var msgEl = document.getElementById(`message`);
  var responseEl = document.getElementById(`response`);

  if (!SERVER_URL) {
    responseEl.innerText = `Hey! You need to set the SERVER_URL variable in ./client/js/script.js first...`;
    return;
  }

  responseEl.innerText = ``;

  var opts = {
    method: `POST`,
    headers: {
      "content-type": `application/json`,
    },
    body: JSON.stringify({ msg: msgEl.value }),
  };

  fetch(SERVER_URL, opts)
    .then((res) => res.json())
    .then((data) => (responseEl.innerText = data.reply))
    .catch((err) => (responseEl.innerText = `Error: ${err.message}`));
}
